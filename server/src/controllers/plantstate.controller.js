const PlantStateModel = require("../models/plantstate.model");


//controllers for plants_state related queries

exports.getPlantsList = (req, res) => {
    try{
        PlantStateModel.getAllPlants((err, plantLocations) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(plantLocations)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getPlantsByStateList = (req, res) => {
    try{
        PlantStateModel.getAllPlantsByState(req.params.state, (err, plantsByState) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(plantsByState)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getPlantsByCategoryList= (req, res) => {
    try{
        PlantStateModel.getAllPlantsByCategory(req.params.category, (err, plantsByCategory) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(plantsByCategory)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getPlantsByStateByCategoryList = (req, res) => {
    try{
        PlantStateModel.getAllPlantsByStateByCategory(req.params.category,req.params.state, (err, plantsByStateByCategory) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(plantsByStateByCategory)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getTopPlantsByNetGenerationList = (req, res) => {
    try{
        PlantStateModel.getTopPlantsByNetGeneration((err, topNetGenerationPlants) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(topNetGenerationPlants)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getTopPlantsByNetGenerationByStateList = (req, res) => {
    try{
        PlantStateModel.getTopPlantsByNetGenerationByState( req.params.state,req.params.top, (err, topNetGenerationPlantsByState) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(topNetGenerationPlantsByState)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getTopPlantsByNetGenerationByCategoryList = (req, res) => {
    try{
        PlantStateModel.getTopPlantsByNetGenerationByCategory( req.params.category,req.params.top, (err, topNetGenerationPlantsByCategory) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(topNetGenerationPlantsByCategory)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getTopPlantsByNetGenerationByStateByCategoryList = (req, res) => {
    try{
        PlantStateModel.getTopPlantsByNetGenerationByStateByCategory(req.params.state,req.params.category, req.params.top, (err, topNetGenerationPlantsByStateByCategory) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(topNetGenerationPlantsByStateByCategory)
            }
        })
    }catch(e){
        console.log(e)
    }
}



exports.getPlantDetails = (req, res) => {
    try{
        PlantStateModel.getPlantDetails(req.params.id,(err, plantDetails) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(plantDetails)
            }
        })
    }catch(e){
        console.log(e)
    }
}

exports.getPlantDetailsByType = (req, res) => {
    try{
        PlantStateModel.getPlantDetailsByType(req.params.id,req.params.type,(err, plantDetailsByType) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(plantDetailsByType)
            }
        })
    }catch(e){
        console.log(e)
    }
}




