var dbConn = require("../../config/db.config");

var States = (states) => {
    this.id = states.id,
    this.state=states.state,
    this.lon=states.lon,
    this.lat=states.lat,
    this.name=states.name
}

function fetchData(query,result,params){
    dbConn.getConnection(function(err, conn) {
        try{
            conn.query(query,params, (err, res)=>{
                if(err){
                    result(err, null);
                }else{
                    result(null, res);
                }
            });
        }catch(e){
            console.log('error',e)
        }finally{
            // Don't forget to release the connection when finished!
            dbConn.releaseConnection(conn);
            console.log('release connection')
        }
     })
}


//fetch all states
States.getAllStates = (result) => {
    const query = "SELECT state as PSTATABB, lon, lat, name from states";
    const params = [];
    fetchData(query,result,params);
}

//fetch details of specified state
States.getStateDetails = (code, result) => {
    const query = "SELECT state as PSTATABB, lon, lat, name from states where state = ?";
    const params = [code];
    fetchData(query,result,params);
}


module.exports = States;
