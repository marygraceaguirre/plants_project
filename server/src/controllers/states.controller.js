const StatesModel = require("../models/states.model");

//controllers for state-related queries

exports.getStatesList = (req, res) => {
    try{
        StatesModel.getAllStates((err, states) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(states);
            }
        })
    }catch(e){
        console.log(e)
    }
    
}

exports.getStateDetailsList = (req, res) => {
    try{
        StatesModel.getStateDetails(req.params.code,(err, states) => {
            if(err){
                res.status(500).send(err);
            }else{
                res.status(200).send(states);
            }
        })
    }catch(e){
        console.log(e)
    }
}