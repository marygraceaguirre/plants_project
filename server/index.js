const express = require("express");
const cors = require("cors");
const app = express();


var corsOptions = {
  origin: "https://bright-wisp-94cae3.netlify.app"
};

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
app.use(express.urlencoded({extended:false}));


let setCache = function (req, res, next) {
  // here you can define period in second, this one is 5 minutes
  const period = 60 * 50

  // you only want to cache for GET requests
  if (req.method == 'GET') {
    res.set('Cache-control', `no-cache, must-revalidate`)
    // res.setHeader('Access-Control-Allow-Origin', 'https://bright-wisp-94cae3.netlify.app');
  } else {
    // for the other requests set strict no caching parameters
    res.set('Cache-control', `no-store`)
  }

  next()
}

app.use(setCache);


const plantStateRoutes = require("./src/routes/plantstate.route");

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to the application." });
});

app.use('/api/v1/states', plantStateRoutes);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});